1. Хеллоу ворлд

//пакет для запуска программы
package main

//пакет ввода вывода
import "fmt"

//объявление функции
func main() {
	
	//использование встроенной функции из пакета fmt
    fmt.Println("Hello, Go!")
}

2. Циклы
	for i := 0; i < 3; i++ {
		fmt.Println("I like Go!")
	}
	
	без скобок
	
	
3. Типы чисел
uint - беззнаковое целое число
int - знаковое число
размерность 8-64 бит
float32/64 - плавающая точка
для повседневной работы используются int/float


4. Строки
Строковые литералы либо "", либо ``
количество байт в строке (len("hello"))

Строки хранятся в виде байтов поэтому надо представлять байты ввиде строки если нужно получить символ
fmt.Println(string("Hello Go"[0]))

5. Переменные 
для переменных используется var nameString
для задания переменных
var hello string
var a = 10

можно объявлять набором
var (
	name string = 'dima'
	age int = 23
)

6. Арифметика
если делятся два int то результат округляется до целого
Арифметические операторы применяются к числовым значениям и дают результат того же типа, что и первый операнд.

7. Ввод через консоль
Когда мы забиваем данные через консоль то делаем ссылку на переменную
//создаем переменную
var name string
//при вводе значения записываем в name
fmt.Scan(&name)
код функции main
```
	var name string
	var age int
	fmt.Print(`Введите имя: `)
	fmt.Scan(&name)
	fmt.Print(`Введите возраст: `)
	fmt.Scan(&age)

	fmt.Println(name, age)
```

Вывод в консоли
fmt.Print - в одну строку
fmt.Println - с переносом строки

Степик
На вход дается означает fmt.Scan для считывания того что дается на вход

Константы
===
Константы как и в остальных языках хранят неизменяемые значения
для определения констант также используется const

```
const pi float64 = 3.1415
```
можно задавать несколько констант сразу

```
package main

import "fmt"

const (
	A int = 45
	B
	C float32 = 32.2
	D
)

func main() {
	fmt.Println(A, B, C, D)
}
```
iota
iota идентификатор Go используется в объявлениях констант для упрощения определений увеличивающихся чисел

```
const (
	Sunday = iota
	Monday
	Tuesday
	Wednesday
	Thursday
	Friday
	Saturday
)

func main() {
	fmt.Println(Sunday)   // вывод 0
	fmt.Println(Saturday) // вывод 6
}
```
грубо говоря это автоматическая раскладка по номеру элемента в возрастающем порядке начиная с нуля

_ - пропуск элемента

Условные конструкции
===
```
if a<b {
	printLn ('a меньше б')
}
```

```
if a < b {
    fmt.Println("a меньше b")
} else if a > b {
    fmt.Println("a больше b")
}
```

switch операторы

```
switch i {
case 0: fmt.Println("Zero")
case 1: fmt.Println("One")
case 2: fmt.Println("Two")
default: fmt.Println("Unknown Number")
}
```
в go нет нужды писать break, для проваливания по уровню ниже нужно добавить fallthrough в конце инструкции
```
case 100:
	fmt.Println(100)
	fallthrough
```	

Количество символов
fmt.Println("Length of number", len(strconv.Itoa(num)))

получение символа строки
fmt.Printf("%c", str[0])
или через индекс
fmt.Printf(numConv[0:1])

Циклы
====
for - единственная конструкция для циклов в гоу
```
	sum := 0
	for i := 1; i < 10; i++ {
		//fmt.Println(i)
		sum += i
	}
	//сумма элементов
	fmt.Println(sum)
```
аналог while 
```
var i = 1
for i < 10{
    fmt.Println(i * i)
    i++
}
```

Форматированый вывод
====
для вывода например двух знаков после запятой используется printf, который принимает сначала строку форматирования, он же спецификатор, потом переменную для вывода

```
	var a rune = 'S'
	fmt.Printf("%q", a)
```
rune это альяс int32


Массивы 
===
объявление массива
var a [3]int
fmt.Println(a) / [0,0,0]
массивы разной длины относятся к разным типам, также не могут без приведения взаимодействовать int32 и int64

var a [3]int = [3]int{1, 2, 3}
b := [3]int{1, 2, 3}
//неявное указание длины массива
c := [...]int{1, 2, 3}
//первый элемент массива 12
d := [3]int{1: 12}

Обращение к элементам массива 
===
Стандартно через индексы 
var numbers [5]int =[5]int{1,2,3,4,5}    
fmt.Println(numbers[0]) 


и циклом через len
```
a := [5]int{1, 2, 3, 4, 5}
fmt.Println(a) // [1 2 3 4 5]
for i := 0; i < len(a); i++ {
}
```

аналог foreach
```
//array - length 5
a := [5]int{1, 2, 3, 4, 5}
fmt.Println(a) // [1 2 3 4 5]

//idx == key, elem = value, a - array
for idx, elem := range a {
    fmt.Printf("Элемент с индексом %d: %d\n", idx, elem)
}	
```
Форматированный вывод числа хардкод через строку
```
f.Printf("%c%c%c", a[2], a[1], a[0])
```

Срез (динамический массив)
===
Объявление пустого/динамического среза
```
var a []int
c := []int{1, 2, 3}
//элемент 1 - 12 
d := []int{1: 12} // [0, 12]
```
либо через функцию make
```
make([]T, length, capacity)
a := make([]int, 10, 10) // [0 0 0 0 0 0 0 0 0 0]
fmt.Println(a)
```

Оператор среза
===
Сделать срез из массива очень просто
```
initialUsers := [8]string{"Bob", "Alice", "Kate", "Sam", "Tom", "Paul", "Mike", "Robert"} // базовый массив

users1 := initialUsers[2:6] // с 3-го по 6-й //сделает срез users1 [Kate Sam Tom Paul]
```

Встроенные операторы для работы со срезами
```
func append(slice []Type, elems ...Type) []Type

a := []int{1, 2, 3}
a = append(a, 4, 5)
```
append - добавляет данные в конец файла

В golang нет функции удаления элемента из среза, для этого используется append


Функции
===
```
func main() {
   hello()
}

func hello() {
    fmt.Println("Hello World")
}
```
Как и в остальных яп функция принимает параметры
```
package main
import "fmt"
 
func main() {
    add(4, 5)   // x + y = 9
    add(20, 6)  // x + y = 26
}
 
func add(x int, y int){
    var z = x + y
    fmt.Println("x + y = ", z)
}

```
Если аргументы передаются по значению то они копируются

Как и в других яп можно возвращать результат из функции
```
//псевдокод
func имя_функции (список_параметров) тип_возвращаемого_значения {
    выполняемые_операторы
    return возвращаемое_значение
}

package main

import "fmt"

func main() {
	var a = add(4, 5)
	var b = add(20, 6)
	fmt.Println(a)
	fmt.Println(b)
}

func add(x, y int) int {
	return x + y
}

```

golang может возвращать несколько значений
```
func add(x, y int, firstName, lastName string) (int , string) {
    var z int = x + y
    var fullName = firstName + " " + lastName
    return z, fullName
}
```
можно присваивать возврат значений сразу двум переменным
```
var age, name = add(4,5, "tom", "sipson")
```

неограниченное количество переменных при передаче в функцию 
```
func sumInt(nums ...int) (int, int) {
	//code here
}
``````
Пакеты
====
Пакеты служат для разделения кода на отдельные части или модули
пакеты делятся на два типа
executable и reusalbe (либы)
пакеты импортируются с помощью import "fmt"
таким образом можно импортировать встроенные и свои пакеты

если нужно присвоить кастомное имя пакету
```
package main

import custom "fmt"

func main() {
	custom.Println("Hello!")
}

``
Так как в гоу нет ооп, поэтому классов тут нет, есть пакеты, большие аппки принято делить на пакеты
Без импорта можно вызывать функции из другого файла в одном пакете.
Для запуска программы выше необходимо указать все файлы пакета main через пробел:
```
go run main.go main2.go
```

