module example.com/m/v2

go 1.21.5

require (
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/rs/zerolog v1.31.0 // indirect
	github.com/semyon-dev/stepik-go v0.0.0-20220105174409-1890d90ede90 // indirect
	golang.org/x/sys v0.16.0 // indirect
)
