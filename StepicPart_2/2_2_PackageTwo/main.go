package main

import (
	"fmt"

	"github.com/semyon-dev/stepik-go"
)

func main() {
	a := 100
	LetsGo(a)
	fmt.Print(a, "")
	stepik.LetsGo(a)
}

func LetsGo(b int) {
	b++
}
