//https://stepik.org/lesson/228838/step/8?unit=201372

package main

import "fmt"

func main() {
	var x, y, z int

	fmt.Println("Input x == 0 or 1")
	fmt.Scan(&x)

	fmt.Println("Input y x == 0 or 1")
	fmt.Scan(&y)

	fmt.Println("Input z x == 0 or 1")
	fmt.Scan(&z)

	if (x >= 0 && x <= 1) && (y >= 0 && y <= 1) && (z >= 0 && z <= 1) {
		fmt.Println(vote(x, y, z))
	} else {
		fmt.Println("error: incorrect value")
	}
}

func vote(x int, y int, z int) int {
	arr := []int{x, y, z}

	maxcount := 0
	var eltMax int

	for i := 0; i < len(arr); i++ {
		count := 0
		for j := 0; j < len(arr); j++ {
			if arr[j] == arr[i] {
				count++
			}
		}

		if (count > maxcount) {
			maxcount = count;
			eltMax = arr[i]
		}
	}

	return eltMax;
}
