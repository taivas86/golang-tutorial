package main

import "fmt"

func main() {
	var N int
	fmt.Println("Input N")
	fmt.Scan(&N)
	fmt.Println(fibonacci(N))
}

func fibonacci(n int) int {
	if n <= 1 {
		return n
	}
	var n2, n1 = 0, 1

	for i := 2; i <= n; i++ {
		n2, n1 = n1, n1+n2
	}

	return n1
}

