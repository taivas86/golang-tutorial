// https://stepik.org/lesson/228838/step/7?unit=201372
package main

import (
	"fmt"
)

func main() {
	fmt.Println(minimumFromFour())
}

func minimumFromFour() int {
	var N int

	fmt.Scan(&N)
	min := N

	//stack
	//min := int(^uint(0) >> 1)

	i := 0
	for i < 4 {
		fmt.Println("Input N")
		fmt.Scan(&N)
		if N < min {
			min = N
		}
		i++
	}

	return min
}
