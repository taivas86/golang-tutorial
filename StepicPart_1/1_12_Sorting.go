package main

import "fmt"

func main() {
	array := [5]int{}
	var a int
	for i := 0; i < 5; i++ {
		fmt.Println("Enter five nums")
		fmt.Scan(&a)
		array[i] = a
	}

	lenA := len(array)

	for i := 0; i < lenA-1; i++ {
		for j := 0; j < lenA-i-1; j++ {
			if array[j] > array[j+1] {
				array[j], array[j+1] = array[j+1], array[j]
			}
		}
	}
	fmt.Println(array[lenA-1])
}
