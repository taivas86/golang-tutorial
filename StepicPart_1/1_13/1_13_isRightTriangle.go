// https://stepik.org/lesson/229320/step/5?unit=201906
package main

import (
	"fmt"
	"math"
)

func main() {
	var a, b, c float64
	fmt.Println("Input triangle size a,b,c")
	fmt.Scan(&a, &b, &c)

	if a < b && b < c && a < c {
		cornerC := math.Pow(c, 2)
		sumAB := math.Pow(a, 2) + math.Pow(b, 2)
		if cornerC == sumAB {
			fmt.Println("Прямоугольный")
		} else {
			fmt.Println("Непрямоугольный")
		}
	}
}
