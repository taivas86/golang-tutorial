// https://stepik.org/lesson/229320/step/9?unit=201906
package main

import (
	"fmt"
	"sort"
)

func main() {
	var N int
	var num int
	fmt.Println(`Input loop qnt`)
	fmt.Scan(&N)
	var numSet []int

	//Prepare data structure
	for i := 0; i < N; i++ {
		fmt.Scan(&num)
		numSet = append(numSet, num)
	}
	sort.Ints(numSet)

	//Find mins foreach
	setLen := len(numSet)
	var counter int = 0
	minElt := numSet[0]
	for i := 0; i < setLen; i++ {
		elt := numSet[i]
		if minElt == elt {
			counter++
		}
	}

	fmt.Println(counter)

}
