//https://stepik.org/lesson/229320/step/11?unit=201906

package main

import "fmt"

func main() {
	var a int
	var b int
	fmt.Println("Input a")
	fmt.Scan(&a)
	fmt.Println("Input b")
	fmt.Scan(&b)

	if a > b {
		fmt.Println("NO")
	}

	var chunkArr []int
	for i := a; i <= b; i++ {
		if i == 0 || i%7 == 0 {
			chunkArr = append(chunkArr, i)
		}
	}

	chArrLen := len(chunkArr)

	if chArrLen > 0 {
		fmt.Println(chunkArr[chArrLen-1])
	} else {
		fmt.Println("NO")
	}

}
