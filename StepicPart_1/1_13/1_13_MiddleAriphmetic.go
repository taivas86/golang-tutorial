// https://stepik.org/lesson/229320/step/7?unit=201906
package main

import "fmt"

func main() {
	var a, b float64
	fmt.Scan(&a, &b)
	middle := (a + b) / 2
	fmt.Println(middle)
}
