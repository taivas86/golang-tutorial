package main

import (
	"fmt"
	"strconv"
)

func main() {
	var n int
	var str string
	fmt.Println("Введите число N")
	fmt.Scan(&n)

	m := 1
	for m <= n {
		str += strconv.Itoa(m) + " "
		m = m * 2
	}

	fmt.Println(str)
}
