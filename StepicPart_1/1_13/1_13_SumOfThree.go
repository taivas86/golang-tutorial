// https://stepik.org/lesson/229320/step/2?unit=201906
package main

import (
	"fmt"
	"strconv"
)

func main() {
	var num int
	fmt.Scan(&num)
	var numLen int = len(strconv.Itoa(int(num)))
	sum := 0

	if numLen <= 3 {
		for num > 0 {
			sum += num % 10
			num /= 10
		}
	}

	fmt.Println(sum)

}
