// https://stepik.org/lesson/229320/step/14?unit=201906
package main

import "fmt"

func main() {
	var N int
	fmt.Scan(&N)

	if N < 1 {
		fmt.Println("N < 1")
	}

	arr := []int{0, 1}
	for i := 2; ; i++ {
		next := arr[i-1] + arr[i-2]

		if next == N {
			fmt.Println(i)
			break
		} else if next > N {
			fmt.Println(-1)
		}
		arr = append(arr, next)
	}
}
