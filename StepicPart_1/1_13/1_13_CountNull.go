// https://stepik.org/lesson/229320/step/8?unit=201906
package main

import "fmt"

func main() {
	var N int
	var num int

	fmt.Println("Input N for loop")
	fmt.Scan(&N)

	var count int = 0
	for i := 0; i < N; i++ {
		fmt.Scan(&num)
		if num == 0 {
			count++
		}
	}

	fmt.Println(count)
}
