// https://stepik.org/lesson/229320/step/15?unit=201906
package main

import (
	"fmt"
	"strconv"
)

func main() {
	var N int64
	fmt.Scan(&N)
	fmt.Println(strconv.FormatInt(N, 2))
}
