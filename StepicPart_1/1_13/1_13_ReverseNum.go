// https://stepik.org/lesson/229320/step/3?unit=201906
package main

import (
	"fmt"
	"strconv"
)

func main() {
	var num int
	var nullState bool
	var res int = 0

	fmt.Println("Input digit")
	fmt.Scan(&num)

	var numLen = len(strconv.Itoa(int(num)))

	//Check last digit not null
	var lastDigit int = num % 10
	if lastDigit != 0 {
		nullState = false
	}

	if numLen <= 3 && nullState != true {
		for num > 0 {
			for num > 0 {
				remainder := num % 10
				res = (res * 10) + remainder
				num /= 10
			}
		}
	}

	fmt.Println(res)
}
