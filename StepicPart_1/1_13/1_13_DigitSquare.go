// https://stepik.org/lesson/229320/step/10?unit=201906

package main

import "fmt"

func main() {
	var N int
	fmt.Println("Input Number")
	fmt.Scan(&N)

	if N <= 10e7 {
		for N > 10 {
			var sum int
			for N > 0 {
				sum += N % 10
				N /= 10
			}

			N = sum
		}
		fmt.Println(N)
	} else {
		fmt.Println("Big numb")
	}

}
