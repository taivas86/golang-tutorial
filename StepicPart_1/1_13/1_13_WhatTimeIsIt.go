// https://stepik.org/lesson/229320/step/4?unit=201906
package main

import "fmt"

func main() {
	var secQnt int
	//fmt.Println(`Input seconds`)
	fmt.Scan(&secQnt)

	secQnt = (secQnt % (24 * 3600))
	hours := secQnt / 3600

	secQnt %= 3600
	minutes := secQnt / 60

	fmt.Println("It is", hours, "hours", minutes, "minutes.")

}
