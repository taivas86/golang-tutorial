package main

import (
	"fmt"
)

func main() {
	var N int
	fmt.Printf("Input number")
	fmt.Scan(&N)

	var D int
	fmt.Println("Delete num from")
	fmt.Scan(&D)

	var chunk []int
	for N > 0 {
		chunk = append(chunk, N%10)
		N = N / 10
	}

	var tmp []int
	for _, i := range chunk {
		if chunk[i] != D {
			tmp = append(tmp, chunk[i])
		}
	}

	fmt.Println(tmp)
}
