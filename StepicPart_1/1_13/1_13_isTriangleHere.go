// https://stepik.org/lesson/229320/step/6?unit=201906
package main

import "fmt"

func main() {
	var a, b, c int
	fmt.Println("Input triangle sizes: a, b, c")
	fmt.Scan(&a, &b, &c)

	if (a+b >= c) && (b+c >= a) && (a+c >= b) {
		fmt.Println("Существует")
	} else {
		fmt.Println("Не существует")
	}
}
