// https://stepik.org/lesson/228265/step/13?unit=200798
package main

import "fmt"

func main() {
	var slice []int
	var N int
	var elt int
	fmt.Println("Enter N")
	fmt.Scan(&N)

	for i := 0; i < N; i++ {
		fmt.Scan(&elt)
		slice = append(slice, elt)
	}

	fmt.Println(slice[3])
}
