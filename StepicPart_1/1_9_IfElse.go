//https://stepik.org/lesson/232593/step/5?unit=205068

package main

import "fmt"

func main() {
	var n int32
	fmt.Scan(&n)

	if n == 0 {
		fmt.Println(`Ноль`)
	}

	if n > 0 {
		fmt.Println(`Число положительное`)
	}

	if n < 0 {
		fmt.Println(`Число отрицательное`)
	}
}
