// https://stepik.org/lesson/232593/step/9?unit=205068
package main

import "fmt"

func main() {
	var year int32
	fmt.Println("Enter year: ")
	fmt.Scan(&year)

	if year%400 == 0 || (year%4 == 0 && year%100 != 0) {
		fmt.Printf("YES")
	} else {
		fmt.Printf("NO")
	}
}
