//https: //stepik.org/lesson/228263/step/3?unit=200796

package main

import "fmt"

func main() {
	var (
		start int
		end   int
	)

	fmt.Println("Enter a")
	fmt.Scan(&start)
	fmt.Println("Enter b")
	fmt.Scan(&end)

	sum := 0
	for i := start; i <= end; i++ {
		sum += i
	}

	fmt.Println(sum)
}
