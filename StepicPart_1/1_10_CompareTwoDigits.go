package main

import "fmt"

func main() {
	var a int
	var b int
	var c int
	var d int

	fmt.Scan(&a)
	fmt.Scan(&b)

	for a > 0 {
		d := a % 10
		a := a / 10
		c := b

		for c > 0 {
			if c%10 == d {
				println(d, ' ')
				break
			}
			c := c / 10
		}

	}

}
