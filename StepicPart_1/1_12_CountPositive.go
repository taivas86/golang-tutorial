package main

import "fmt"

func main() {
	var N int
	var counter int
	var elt int
	//fmt.Println("Количество шагов счетчика")
	fmt.Scan(&N)

	if N >= 1 && N <= 100 {
		for i := 0; i < N; i++ {
			fmt.Scan(&elt)
			if elt >= 0 {
				counter++
			}
		}
	}

	fmt.Println(counter)

}
