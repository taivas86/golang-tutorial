// https://stepik.org/lesson/232593/step/7?unit=205068
package main

import (
	"fmt"
	"strconv"
)

func main() {
	var num uint32
	fmt.Println("Введите число")
	fmt.Scan(&num)
	numConv := strconv.Itoa(int(num))
	/*
		через спецификатор формат
		fmt.Printf("%c", str[0])
	*/
}
