//https://stepik.org/lesson/228261/step/16?unit=200794

package main

import "fmt"

func main() {
	var degree uint16
	var hour uint16
	var minute uint16
	fmt.Scan(&degree)

	hour = degree / 30
	minute = 2 * (degree % 30)

	fmt.Println(`It is`, hour, `hours`, minute, `minutes.`)
}
