//https: //stepik.org/lesson/228263/step/9?unit=200796

package main

import "fmt"

func main() {
	var x int
	var n int
	var y int

	fmt.Println("Init depo value")
	fmt.Scan(&x)

	fmt.Println("Depo intrest")
	fmt.Scan(&n)

	fmt.Println("End depo value")
	fmt.Scan(&y)

	years := 0

	for x < y {
		x += x * n / 100
		years++
	}

	fmt.Println(years)
}
