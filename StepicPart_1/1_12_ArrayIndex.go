package main

import "fmt"

func main() {
	var numbers [8]int = [8]int{3, 7, 1, 9, 5, 4, 2, 6}
	var lenArray int = len(numbers)

	for i := 0; i < lenArray; i++ {
		var elt int = numbers[i]
		fmt.Println("Э=", elt, "||", "И=", i)
	}

}
