// https://stepik.org/lesson/228263/step/4?unit=200796
package main

import (
	"fmt"
	"strconv"
)

func main() {

	var a int
	var b int
	var sum int = 0

	fmt.Println("Количество итераций цикла")
	fmt.Scan(&a)

	for i := 0; i < a; i++ {
		fmt.Println("Введите число №", i)
		fmt.Scan(&b)
		if b%8 == 0 && len(strconv.Itoa(b)) == 2 {
			sum += b
		}
	}

	fmt.Println(sum)

}
