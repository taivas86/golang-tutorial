// https://stepik.org/lesson/232593/step/8?unit=205068
package main

import (
	"fmt"
	"strconv"
)

func getSumOfNums(chunk int) int {
	sum := 0
	for i := chunk; i > 0; i = i / 10 {
		sum = sum + (i % 10)
	}

	return sum
}

func main() {
	var ticketNumber int32
	fmt.Println("Enter ticket number")
	fmt.Scan(&ticketNumber)

	var numLen int = len(strconv.Itoa(int(ticketNumber)))

	if numLen == 6 {
		var firstDigs int = int(ticketNumber) / 1e3
		var lastDigs int = int(ticketNumber) % 1e3

		if getSumOfNums(firstDigs) == getSumOfNums(lastDigs) {
			fmt.Println("YES")
		} else {
			fmt.Println("NO")
		}
	}

}
