//https://stepik.org/lesson/228263/step/2?unit=200796

package main

import (
	"fmt"
)

func main() {
	for i := 1; i <= 10; i++ {
		fmt.Println(i * i)
	}
}
