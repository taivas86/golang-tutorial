//https://stepik.org/lesson/228263/step/7?unit=200796

package main

import "fmt"

func main() {
	var n int
	var c int
	var d int

	fmt.Println(`Введите n`)
	fmt.Scan(&n)
	fmt.Println(`Введите кратное с`)
	fmt.Scan(&c)
	fmt.Println(`Введите не кратное d`)
	fmt.Scan(&d)

	for i := 1; i <= n; i++ {
		if i%c == 0 && i%d != 0 {
			fmt.Println(i)
			break
		}
	}

}
