package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	var N int
	var array []int
	var elt int

	fmt.Println("Input numbers of step")
	fmt.Scan(&N)

	for i := 0; i < N; i++ {
		fmt.Scan(&elt)
		if i%2 == 0 {
			array = append(array, elt)
		}
	}

	fmt.Println("======")

	var nums []string
	for _, i := range array {
		nums = append(nums, strconv.Itoa(i))
	}

	fmt.Println(strings.Join(nums, " "))
}
