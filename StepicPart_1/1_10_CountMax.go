// https://stepik.org/lesson/228263/step/5?unit=200796
package main

import "fmt"

func main() {
	var n int
	var max int
	var count int

	for {
		fmt.Scan(&n)
		if n == 0 {
			break
		}
		if n > max {
			max = n
			count = 1
		} else if n == max {
			count++
		}
	}

	fmt.Println(count)
}
