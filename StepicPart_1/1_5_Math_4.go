package main

import "fmt"

func main() {
	var N uint16
	fmt.Scan(&N)
	fmt.Println(N % 1e1)
}
