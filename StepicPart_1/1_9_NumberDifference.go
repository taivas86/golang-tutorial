//https://stepik.org/lesson/232593/step/6?unit=205068

package main

import (
	"fmt"
	"strconv"
)

func main() {
	var num int
	fmt.Println("Введите целое число")
	fmt.Scan(&num)

	var numLen int = len(strconv.Itoa(num))

	if numLen == 3 {
		var a int = num / 100
		var b int = (num / 10) % 10
		var c int = num % 10

		if (a != b && a != c) &&
			(b != a && b != c) &&
			(c != a && c != b) {
			{
				fmt.Println("YES")
			}
		} else {
			fmt.Println("NO")
		}
	}
}
